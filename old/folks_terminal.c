//Tiffany Antopolski
//file: folks_terminal.c

/* This program uses folks to get the list of contacts and prints them to the terminal. */

/*
To compile:
----------
gcc -Wall -g folks_terminal.c -o ft `pkg-config --cflags --libs gtk+-3.0 folks` 
*/


#include <gtk/gtk.h>
#include <folks/folks.h>

static void
ind_changed (FolksIndividualAggregator *individual_aggregator,
             GeeSet *added,
             GeeSet *removed,
             gchar  *message,
             FolksPersona *actor,
             FolksGroupDetailsChangeReason reason,
             gpointer user_data)
{
  gpointer *added_array;
  gpointer *removed_array;
  gint n_added;
  gint n_removed;
  gint i;

  added_array = gee_collection_to_array (GEE_COLLECTION (added), &n_added);
  removed_array = gee_collection_to_array (GEE_COLLECTION (removed), &n_removed);
  g_print("a: %i, %i\n", n_added, n_removed);

  for (i = 0; i<n_added; i++)
  {
   g_print ("%s\n",folks_alias_details_get_alias (FOLKS_ALIAS_DETAILS (added_array[i])));
  }

  g_free (added_array);
  g_free (removed_array);
}

static void
prepare_done (GObject      *source,
              GAsyncResult *result,
              gpointer      user_data)
{
  g_print ("prepared_done\n");
}

int main (void)
{
   FolksIndividualAggregator *aggregator;
   gtk_init(NULL, NULL);
   aggregator = folks_individual_aggregator_new ();
   g_signal_connect (aggregator,"individuals-changed", G_CALLBACK (ind_changed), NULL);
   folks_individual_aggregator_prepare (aggregator, prepare_done, NULL);
   gtk_main();
   return 0;
}
