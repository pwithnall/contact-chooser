//file: contact_chooser.vala

using Gtk;
using Folks;

public class ContactSelector : Dialog
{
  ListStore listmodel;
  TreeSelection selection;
  IndividualAggregator aggregator;

  public ContactSelector ()
    {
      title = "Invite participants to \'meeting\':";
      add_buttons (Stock.CANCEL, ResponseType.CANCEL, "Share", ResponseType.OK);
      set_border_width (2);
      set_size_request (350, 300);

      var search = new Entry ();
      search.set_icon_from_stock (EntryIconPosition.SECONDARY, Stock.FIND);
      search.placeholder_text = "Search or enter an email address";

      var scrolled_window = new ScrolledWindow (null, null);
      scrolled_window.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
      scrolled_window.expand = true;

      var view = new TreeView ();
      view.headers_visible = false;
      view.set_search_column (2);
      view.set_search_entry (search);
      view.enable_search = true;

      scrolled_window.add (view);

      var permissions = new ComboBoxText ();
      permissions.append_text ("Can Edit");
      permissions.append_text ("View Only");
      permissions.active = 0;

      var email_notify = new CheckButton.with_mnemonic ("_Notify new sharers by email");
      email_notify.margin = 10;

      var grid = new Grid ();

      grid.attach (search, 0, 0, 2, 1);
      grid.attach (scrolled_window, 0, 1, 2,1);
      grid.attach (permissions, 0, 2, 1, 1);
      grid.attach (email_notify, 1, 2, 1, 1);

      (get_content_area () as Container).add (grid);
      setup_treeview (view);
      aggregator = new Folks.IndividualAggregator ();
      aggregator.individuals_changed.connect (ind_changed);
      aggregator.prepare.begin ();
    }

  void add_individual (Folks.Individual person)
    {
      TreeIter row;
      //print ("%s\n", person.alias);

      listmodel.append (out row);
      listmodel.set (row, 0, person.id);
      listmodel.set (row, 2, person.alias);

      var avatar = person.avatar;
      if (avatar != null)
        {
          try
            {
              var gpb = new Gdk.Pixbuf.from_file (avatar.get_path ());
              gpb = gpb.scale_simple (32, 32, Gdk.InterpType.BILINEAR);
              listmodel.set (row, 1, gpb);
            }
          catch
            {
              // if it didn't work, no icon will be rendered.
           }
        }
    }

  void remove_individual (Folks.Individual person)
    {
      TreeIter row;
      string id;

      if (listmodel.get_iter_first (out row))
        {
          do
            {
              listmodel.get (row, 0, out id);
              if (person.id == id)
                {
                  listmodel.remove (row);
                  return;
                }
            } while (listmodel.iter_next (ref row));
        }
        assert_not_reached ();
    }

  private void ind_changed (Folks.IndividualAggregator ind,
      Gee.Set<Folks.Individual> added,
      Gee.Set<Folks.Individual> removed,
      string? message,
      Folks.Persona? actor,
      Folks.GroupDetails.ChangeReason reason)
    {
      foreach (var person in added)
        {
          add_individual (person);
        }

      foreach (var person in removed)
        {
          remove_individual (person);
        }
    }

  private void setup_treeview (TreeView view)
    {
      listmodel = new ListStore (3, typeof (string), typeof (Gdk.Pixbuf), typeof (string));
      listmodel.set_sort_column_id (2, SortType.ASCENDING);
      view.set_model (listmodel);
      var crp = new CellRendererPixbuf ();

      /* test */
      //view.insert_column_with_attributes (-1, "Id", new CellRendererText (), "text", 0);

      view.insert_column_with_attributes (-1, "Avatar", crp, "pixbuf", 1);
      view.insert_column_with_attributes (-1, "Contact", new CellRendererText (), "text", 2);

      selection = view.get_selection ();
      selection.set_mode (SelectionMode.MULTIPLE);
    }

  private void invite (TreeModel model, TreePath path, TreeIter row)
    {
      string alias;
      model.get (row, 2, out alias); //0th column, but 2 to show result for now.
      print ("%s invited.\n", alias);
    }


  protected override void response (int id)
    {
      switch (id)
        {
          case ResponseType.OK:
            print ("%i rows selected.\n", selection.count_selected_rows ());
            selection.selected_foreach (invite);
            break;
          default:
            destroy ();
          break;
        }
    }
}
