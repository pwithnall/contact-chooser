//file: participants.vala

using Gtk;

public class Participants : Grid
{
  public static int main (string[] args)
    {
      init (ref args);

      var sample = new ContactSelector ();
      sample.show_all ();
      Gtk.main ();
      return 0;
    }
}
